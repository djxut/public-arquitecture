version = ":1.0.0"
project = "cgardev/djuxt-"

build:
	docker-compose build djuxt-base-python-36
	docker-compose build djuxt-base-django-2

push:
	docker tag ${project}base-python-36 ${project}base-python-36${version}
	docker push ${project}base-python-36${version}

	docker tag ${project}base-django-2 ${project}base-django-2${version}
	docker push ${project}base-django-2${version}